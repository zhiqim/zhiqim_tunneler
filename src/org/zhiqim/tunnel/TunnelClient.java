/*
 * 版权所有 (C) 2015 知启蒙(ZHIQIM) 保留所有权利。[遇见知启蒙，邂逅框架梦，本文采用木兰宽松许可证第2版]
 * 
 * https://zhiqim.org/project/zhiqim_products/zhiqim_tunneler.htm
 *
 * Zhiqim Tunneler is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */
package org.zhiqim.tunnel;

import java.io.InputStream;
import java.io.OutputStream;
import java.net.Socket;

import org.zhiqim.kernel.util.Closes;

/**
 * 隧道客户端
 *
 * @version v1.5.2 @author zouzhigang 2019-5-31 新建与整理
 */
public class TunnelClient extends TunnelConnection
{
    private final TunnelServer server;
    
    public TunnelClient(TunnelServer server, Socket socket, InputStream input, OutputStream output)
    {
        super("TunnelClient", server.getConnId(), socket, input, output);
        
        this.server = server;
        super.open();
    }

    /********************************************************************************************/
    //连接关闭
    /********************************************************************************************/
    
    @Override /** 线程关闭后 */
    public void closeAfter()
    {
        Closes.closeIgnoreException(socket, input, output);
    }

    /*********************************************************************************/
    //消息处理
    /*********************************************************************************/
    
    @Override /** 接受消息，转发到服务端 */
    public void receive(InputStream in) throws Exception
    {
        server.send(in);
    }
    
    @Override /** 退出 */
    public void exit()
    {
        server.close();
    }
    
    @Override /** 异常 */
    public void exception(Throwable e)
    {
        server.close();
    }
}
