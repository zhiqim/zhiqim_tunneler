### 什么是“Zhiqim Tunneler”？
---------------------------------------
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;“Zhiqim Tunneler”即知启蒙通讯隧道工程，用于TCP端口异步消息转发，最早应用的移动短信业务的消息转发，因为移动公司对IP地址要求鉴权，因此需要在指定IP服务器上安装隧道工程转发。

<br>

### Zhiqim Tunneler有什么优点？
---------------------------------------
1、目标系统要求IP鉴权，但更换IP提交审核时间长且麻烦，选择tunneler绝对错不了。<br>
2、tunneler只依赖JDK和Zhiqim Framework，安装简单到爆，特别是有经验的技术人员，配置好boot.home和本地监听端口和目标IP和端口，一键启动（zhiqim.exe/zhiqim.lix）。<br>
3、Windows/Linux都支持，并且经过大量压力测试，可靠性有保障。<br>
<br>

### Zhiqim Tunneler安装指南
---------------------------------------
1、要求JDK1.7+,其详细安装教程请前往[【JDK安装教程】](https://zhiqim.org/document/bestcase/jdk.htm)。<br>
2、进往下载发行版：下载请点击[【ZhiqimTunneler发行版】](https://zhiqim.org/project/zhiqim_products/zhiqim_tunneler/release.htm)。<br>
<br>

### 知启蒙技术框架与交流
---------------------------------------
![知启蒙技术框架架构图](https://zhiqim.org/project/images/101431_93f5c39d_2103954.jpeg "知启蒙技术框架架构图.jpg")<br><br>
QQ群：加入QQ交流群，请点击[【458171582】](https://jq.qq.com/?_wv=1027&k=5DWlB3b) <br><br>
教程：欲知更多Zhiqim Tunneler，[【请戳这里】](https://zhiqim.org/project/zhiqim_products/zhiqim_tunneler/tutorial/index.htm)
