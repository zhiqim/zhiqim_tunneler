/*
 * 版权所有 (C) 2015 知启蒙(ZHIQIM) 保留所有权利。[遇见知启蒙，邂逅框架梦，本文采用木兰宽松许可证第2版]
 * 
 * https://zhiqim.org/project/zhiqim_products/zhiqim_tunneler.htm
 *
 * Zhiqim Tunneler is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */
package org.zhiqim.tunnel;

import java.io.IOException;
import java.net.InetAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.SocketException;
import java.util.ArrayList;
import java.util.List;

import org.zhiqim.kernel.Global;
import org.zhiqim.kernel.constants.SignConstants;
import org.zhiqim.kernel.control.ThreadServicer;
import org.zhiqim.kernel.logging.Log;
import org.zhiqim.kernel.logging.LogFactory;
import org.zhiqim.kernel.schedule.Interval;
import org.zhiqim.kernel.schedule.Task;
import org.zhiqim.kernel.util.Sockets;
import org.zhiqim.kernel.util.Validates;

/**
 * 隧道服务
 *
 * @version v1.5.2 @author zouzhigang 2019-5-31 新建与整理
 */
public class Tunneler extends ThreadServicer implements Task, SignConstants
{
    private static final Log log = LogFactory.getLog(Tunneler.class);
    
    //隧道服务列表
    private final List<TunnelServer> serverList;
    
    //隧道服务端参数
    private String serverHost;
    private int serverPort;
    private int serverTimeout;
    
    //隧道监听端参数
    private String listenHost;
    private int listenPort;
    private int listenBacklog;
    private ServerSocket listenSocket;
    
    //隧道每分钟任务
    private Interval interval;
    
    /** 默认构造函数 */
    public Tunneler()
    {
        serverList = new ArrayList<>();
        interval = Interval.shedule(this, 3000, 60 * 1000);
    }
    
    /********************************************************************************************/
    //服务创建&销毁
    /********************************************************************************************/
    
    @Override /** 线程名称 */
    protected String getThreadName()
    {
        return id;
    }
    
    @Override /** 线程启动之前 */
    public boolean openBefore()
    {
        //1.检查服务端配置
        serverHost = Global.getString(id, "serverHost");
        serverPort = Global.getInt(id, "serverPort");
        if (Validates.isEmpty(serverHost) || serverPort == -1)
        {
            log.info("Tunneler请先配置服务器主机和端口");
            return false;
        }
        
        serverTimeout = Global.getInt(id, "serverTimeout", 10);
        log.info("Tunneler配置的服务器[%s:%s]", serverHost, serverPort);
        
        try
        {
            //2.启动监听端配置
            listenHost = Global.getString(id, "listenHost", "0.0.0.0");
            listenPort = Global.getInt(id, "listenPort", 17890);
            listenBacklog = Global.getInt(id, "listenBacklog", 50);
            
            listenSocket = new ServerSocket(listenPort, listenBacklog, InetAddress.getByName(listenHost));
            listenSocket.setReuseAddress(true);
            
            log.info("Tunneler监听端口成功[%s:%s]", listenHost, listenPort);
            return true;
        }
        catch(Throwable e)
        {
            log.info("Tunneler监听端口失败[%s:%s]", listenHost, listenPort);
            return false;
        }
    }
    
    @Override /** 线程关闭之后 */
    public void closeAfter()
    {
        //1.关闭监听
        if (listenSocket != null)
        {
            try{listenSocket.close();}catch (IOException e){}
            listenSocket = null;
        }
        
        //2.关闭所有服务
        synchronized (serverList)
        {
            for (TunnelServer server : serverList)
            {
                server.close();
            }
            serverList.clear();
        }
        
        //3.关闭定时任务
        if (interval != null)
        {
            interval.closeNotInterrupt();
            interval = null;
        }
        
        //4.退出日志
        log.info("Tunneler[%s:%s]退出完成", listenHost, listenPort);
    }
    
    /********************************************************************************************/
    //服务监听连接
    /********************************************************************************************/
    
    @Override
    public void loop()
    {
        try
        {
            Socket socket = listenSocket.accept();
            Sockets.setOption(socket, true, 0, 0, true);
            
            TunnelServer conn = new TunnelServer(this, socket, socket.getInputStream(), socket.getOutputStream());
            if (conn.open())
            {//增加连接
                add(conn);
            }
            else
            {//关闭连接
                conn.close();
            }
        }
        catch (SocketException e)
        {
            if (!_SOCKET_CLOSED_.equalsIgnoreCase(e.getMessage()))
            {//非主动关闭导致的Socket异常
                log.error("Tunneler[%s:%s]SocketException", e, listenHost, listenPort);
            }
        }
        catch (IOException e)
        {
            log.error("Tunneler[%s:%s]IOException", e, listenHost, listenPort);
        }
    }

    /********************************************************************************************/
    //内部调用方法（接收客户端消息/移除连接）
    /********************************************************************************************/
    
    /** 增加连接 */
    private void add(TunnelServer conn)
    {
        synchronized (serverList)
        {
            if (!isRunning()){
                conn.close();
            }else{
                serverList.add(conn);
            }
        }
    }
    
    /** 移除连接 */
    void remove(TunnelServer conn)
    {
        synchronized (serverList)
        {
            serverList.remove(conn);
        }
    }
    
    /********************************************************************************************/
    //每分钟任务
    /********************************************************************************************/
    
    @Override /** 每分钟打印日志 */
    public void execute()
    {
        log.info("共[%s]连接", serverList.size());
    }
    
    /********************************************************************************************/
    //配置参数
    /********************************************************************************************/
    
    public String getServerHost()
    {
        return serverHost;
    }

    public int getServerPort()
    {
        return serverPort;
    }
    
    public int getServerTimeout()
    {
        return serverTimeout;
    }
    
    public String getListenHost()
    {
        return listenHost;
    }

    public int getListenBacklog()
    {
        return listenBacklog;
    }

    public int getListenPort()
    {
        return listenPort;
    }
    
    @Override
    public String toString()
    {
        return new StringBuilder("Tunneler[").append(listenHost).append(":").append(listenPort).append("]").toString();
    }
}
